import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocaleService } from './services/locale.service';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'employee-services';
  rtl;
  loggedIn = false;

  constructor(private router : Router, private _translateService:TranslateService, private _localeService: LocaleService, private _authenticationService : AuthenticationService){}
  
  ngOnInit() {
    console.log(this.router.url);
    this.rtl = true;
    localStorage.setItem('rtl','true');
    $('head').append('<link id="rtl_css" href="/src/css/rtl.css" rel="stylesheet" media="all">'); 
    this._translateService.setDefaultLang('ar');
    console.log("Current Language : " + this._translateService.currentLang);
    if(this._authenticationService.loggedIn())
    {
      this.loggedIn = true;
    }
    else
    {
      this.loggedIn = false;
    }
  }

  switchArabic()
  {
    localStorage.setItem('rtl','true');
    this.rtl = true;
    this._translateService.use('ar');
    $("#rtl_css").attr('href','assets/rtl.css');
  }

  switchEnglish()
  {
    localStorage.setItem('rtl','false');
    this.rtl = false;
    this._translateService.use('en');
    $("#rtl_css").attr('href','');
  }

  logout()
  {
    this._authenticationService.logOut();
    location.reload(true);
  }
  
  login()
  {
    this.router.navigate(['login']);
  }

  
}


