import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { RequestsComponent } from './components/requests/requests.component';
import { CreateUserValidateComponent } from './components/create-user/create-user-validate/create-user-validate.component';
import { CreateUserSuccessComponent } from './components/create-user/create-user-success/create-user-success.component';
import { CreateUserComponent } from './components/create-user/create-user/create-user.component';
import { ConfirmCreateUserComponent } from './components/create-user-confirm/confirm-create-user/confirm-create-user.component';
import { ConfirmCreateUserPasswordComponent } from './components/create-user-confirm/confirm-create-user-password/confirm-create-user-password.component';
import { ResetPasswordRequestComponent } from './components/reset-password/reset-password-request/reset-password-request.component';
import { ConfirmResetPasswordRequestComponent } from './components/reset-password/confirm-reset-password-request/confirm-reset-password-request.component';
import { ChangeAttributesRequestComponent } from './components/change-attributes/change-attributes-request/change-attributes-request.component';
import { ChangeAttributesConfirmComponent } from './components/change-attributes/change-attributes-confirm/change-attributes-confirm.component';
import { ConfirmCreateUserSuccessComponent } from './components/create-user-confirm/confirm-create-user-success/confirm-create-user-success.component';
import { ChangeAttributesSuccessComponent } from './components/change-attributes/change-attributes-success/change-attributes-success.component';
import { ResetPasswordSuccessComponent } from './components/reset-password/reset-password-success/reset-password-success.component';
import { ChangePasswordRequestComponent } from './components/change-password/change-password-request/change-password-request.component';
import { ChangePasswordConfirmComponent } from './components/change-password/change-password-confirm/change-password-confirm.component';
import { ChangePasswordSuccessComponent } from './components/change-password/change-password-success/change-password-success.component';
import { UserService } from './services/user.service';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuard } from './guards/auth.guard';
import { JwtInterceptorService } from './interceptors/jwt-interceptor.service';
import { UnauthorizedErrorInterceptorService } from './interceptors/unauthorized-error-interceptor.service';

import {TranslateModule , TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { FileServiceService } from './services/file-service.service';
import { TrackPendingConfirmComponent } from './components/track-pending/track-pending-confirm/track-pending-confirm.component';
import { TrackPendingRequestComponent } from './components/track-pending/track-pending-request/track-pending-request.component';
import { TrackPendingSuccessComponent } from './components/track-pending/track-pending-success/track-pending-success.component';
import { GreetingComponent } from './components/home/greeting/greeting.component';
import { MyInfoComponent } from './components/home/my-info/my-info.component';
import { HomeComponent } from './components/home/home/home.component';
import { LoadingComponent } from './components/loading/loading.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SidebarComponent,
    TopBarComponent,
    RequestsComponent,
    CreateUserValidateComponent,
    CreateUserSuccessComponent,
    CreateUserComponent,
    ConfirmCreateUserComponent,
    ConfirmCreateUserPasswordComponent,
    ResetPasswordRequestComponent,
    ConfirmResetPasswordRequestComponent,
    ChangeAttributesRequestComponent,
    ChangeAttributesConfirmComponent,
    ConfirmCreateUserSuccessComponent,
    ChangeAttributesSuccessComponent,
    ResetPasswordSuccessComponent,
    ChangePasswordRequestComponent,
    ChangePasswordConfirmComponent,
    ChangePasswordSuccessComponent,
    TrackPendingConfirmComponent,
    TrackPendingRequestComponent,
    TrackPendingSuccessComponent,
    HomeComponent,
    GreetingComponent,
    MyInfoComponent,
    LoadingComponent  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader : {
        provide : TranslateLoader,
        useFactory : HttpLoaderFactory,
        deps : [HttpClient]
      }
    })
  ],
  providers: [
    UserService,
    FileServiceService,
    AuthenticationService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: UnauthorizedErrorInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 

}

export function HttpLoaderFactory(http:HttpClient)
{
  return new TranslateHttpLoader(http, "assets/i18n/", ".json");
}
