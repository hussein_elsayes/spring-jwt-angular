import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { OuService } from 'src/app/services/ou.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserDetails } from 'src/app/models/user-details';
import { HttpErrorResponse } from '@angular/common/http';
import { PasswordValidator } from 'src/app/validators/password-validator';

@Component({
  selector: 'app-change-password-request',
  templateUrl: './change-password-request.component.html',
  styleUrls: ['./change-password-request.component.css']
})

export class ChangePasswordRequestComponent implements OnInit {

  constructor(private fb:FormBuilder, private _userService :UserService,private _ouService:OuService,private _router : Router, private _route :ActivatedRoute) { }
  formInitializing = false;
  
  errorMsg;
  selectedReqId;
  showChangePassRequest = true;
  showConfirm = false;
  showSuccess = false;
  newPass;
  confirmPass;

  userForm = this.fb.group(
    {
      password : ['',[Validators.required]],    
      confirmPassword : ['',[Validators.required]]
    }
    ,{validator : PasswordValidator});

  ngOnInit() {
  }

  onSubmit(userForm)
  {
    this.formInitializing = true;
    this._userService.sendChangePassRequest().subscribe(
      data=>
      { 
        console.log(data);
        this.newPass = this.userForm.get('password').value;
        this.confirmPass = this.userForm.get('confirmPassword').value;
        this.selectedReqId = data.message;
        this.showChangePassRequest = false;
        this.showConfirm = true;
      },
    error =>
    {
      this.formInitializing = false;
        let httpError =  <HttpErrorResponse>error;
        //if (httpError.status != 400)
        //{
          console.log(httpError);
          //this.errorMsg = "Error Executing the request !";
        //}else
        //{
          //this.validationErrors = error.error.errorsWrapper.errors
          //console.log(this.validationErrors);
       // }
       // console.log(httpError.error);
    });
    //console.log(userForm);
  }

}
