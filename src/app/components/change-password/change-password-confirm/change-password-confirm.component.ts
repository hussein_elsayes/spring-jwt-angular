import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-change-password-confirm',
  templateUrl: './change-password-confirm.component.html',
  styleUrls: ['./change-password-confirm.component.css']
})
export class ChangePasswordConfirmComponent implements OnInit {

  constructor(private _router : Router,private _route : ActivatedRoute, private fb: FormBuilder,private _userService :UserService) { }
  formInitializing = false;
  errorMsg;

  @Input('selectedReqId') public selectedReqId;
  @Input('newPass') public newPass;
  @Input('confirmPass') public confirmPass;
  @Output('showConfirmEvent') public showConfirmEvent = new EventEmitter();
  @Output('showSuccessEvent') public showSuccessEvent = new EventEmitter();
  

  validateForm = this.fb.group(
    {
      vcode : ['',[Validators.required,Validators.pattern(/^[0-9]{5}$/)]]
    }
  );

  ngOnInit() {
  }

  onSubmit(event)
  {
    this.formInitializing = true;
    console.log(this.selectedReqId);
    let formData = new FormData();
    formData.append('id',this.selectedReqId);
    formData.append('vcode',this.validateForm.get('vcode').value);
    formData.append('newPassword',this.newPass);
    formData.append('confirmPassword',this.confirmPass);
    this._userService.confirmChangePassRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.errorMsg = null;
        this.showConfirmEvent.emit(false);
        this.showSuccessEvent.emit(true);  
      },
    error =>
    {
      this.formInitializing = false;
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.errorMsg = "Error Executing the request !";
        }else
        {
          this.errorMsg = httpError.error.message;
        }
             
    });
    
  }

}
