import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackPendingSuccessComponent } from './track-pending-success.component';

describe('TrackPendingSuccessComponent', () => {
  let component: TrackPendingSuccessComponent;
  let fixture: ComponentFixture<TrackPendingSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackPendingSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackPendingSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
