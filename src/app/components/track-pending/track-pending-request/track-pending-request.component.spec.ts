import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackPendingRequestComponent } from './track-pending-request.component';

describe('TrackPendingRequestComponent', () => {
  let component: TrackPendingRequestComponent;
  let fixture: ComponentFixture<TrackPendingRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackPendingRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackPendingRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
