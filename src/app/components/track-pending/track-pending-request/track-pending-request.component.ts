import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { OuService } from '../../../services/ou.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-track-pending-request',
  templateUrl: './track-pending-request.component.html',
  styleUrls: ['./track-pending-request.component.css']
})
export class TrackPendingRequestComponent implements OnInit {
  constructor(private fb:FormBuilder, private _userService :UserService,private _ouService:OuService,private _router : Router, private _route :ActivatedRoute) { }
  formInitializing = false;
  errorMsg;
  showtTrackPendingRequest = true;
  showConfirm = false;
  showSuccess = false;
  selectedReqId;
  genericError= false;
  ngOnInit() {
  }

  //create the formGroup
userForm = this.fb.group(
  {    
    mobileNo : ['533800945',[Validators.required,Validators.pattern(/^[5][0-9]{8}$/)]],
    nationalId : ['2374258701',[Validators.required,Validators.pattern(/^[1-2][0-9]{9}$/)]],
    employeeId : ['217124',[Validators.required,Validators.pattern(/^[0-9]{1,6}$/)]],
    
  }
);

onSubmit(userForm)
  {
    this.formInitializing = false;
    //prepare formData
    let formData = new FormData();
    formData.append('mobileNo',userForm.get('mobileNo').value);
    formData.append('nationalId',userForm.get('nationalId').value);
    formData.append('employeeId',userForm.get('employeeId').value);
    this._userService.getPendingRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.selectedReqId = data.message;
        this.showtTrackPendingRequest = false;
        this.showConfirm = true;
      },
    error =>
    {
        this.formInitializing = false;
        let httpError =  <HttpErrorResponse>error;
        if (httpError.status != 400)
        {
          this.genericError = true;
        }else
        {
          this.errorMsg = httpError.error.message;
          console.log(httpError.error);
        }
        console.log(error);
    });
    //console.log(userForm);
  }
}
