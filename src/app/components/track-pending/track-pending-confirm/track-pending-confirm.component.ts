import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-track-pending-confirm',
  templateUrl: './track-pending-confirm.component.html',
  styleUrls: ['./track-pending-confirm.component.css']
})
export class TrackPendingConfirmComponent implements OnInit {
  constructor(private _router : Router,private _route : ActivatedRoute, private fb: FormBuilder,private _userService :UserService) { }

  errorMsg;

  @Input('selectedReqId') public selectedReqId;
  @Output('showConfirmEvent') public showConfirmEvent = new EventEmitter();
  @Output('showSuccessEvent') public showSuccessEvent = new EventEmitter();
  

  validateForm = this.fb.group(
    {
      vcode : ['12345',[Validators.required,Validators.pattern(/^[0-9]{5}$/)]]
    }
  );

  ngOnInit() {
  }

  onSubmit(event)
  {
    console.log(this.selectedReqId);
    let formData = new FormData();
    formData.append('id',this.selectedReqId);
    formData.append('vcode',this.validateForm.get('vcode').value);
    this._userService.validateCreateUserRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.errorMsg = null;
        this.showConfirmEvent.emit(false);
        this.showSuccessEvent.emit(true);  
      },
    error =>
    {
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.errorMsg = "Error Executing the request !";
        }else
        {
          this.errorMsg = httpError.error.message;
        }
             
    });
    
  }
}
