import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackPendingConfirmComponent } from './track-pending-confirm.component';

describe('TrackPendingConfirmComponent', () => {
  let component: TrackPendingConfirmComponent;
  let fixture: ComponentFixture<TrackPendingConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackPendingConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackPendingConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
