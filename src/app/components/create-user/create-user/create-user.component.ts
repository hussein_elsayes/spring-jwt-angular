import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { OuService } from '../../../services/ou.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  constructor(private fb:FormBuilder, private _userService :UserService,private _ouService:OuService,private _router : Router, private _route :ActivatedRoute) { }

  //state variables
formInitializing = true;
submitted = false;
toValidate = false;
completed = false;



returnedReqId;
  //create the formGroup
  userForm = this.fb.group(
    {    
      firstName : ['', [Validators.required,Validators.minLength(3),Validators.maxLength(10),Validators.pattern(/^[A-z ]+$/)]],
      lastName : ['',[Validators.required,Validators.minLength(3),Validators.maxLength(10),Validators.pattern(/^[A-z ]+$/)]],
      arabicName : ['',[Validators.required,Validators.minLength(3),Validators.maxLength(10),Validators.pattern(/^((:?\s*[\u0600-\u06FF]\s*)+)$/)]],
      gender : ['1',[Validators.required,Validators.pattern(/^[1-2]$/)]],
      nationalId : ['',[Validators.required,Validators.pattern(/^[1-2][0-9]{9}$/)]],
      mobile : ['',[Validators.required,Validators.pattern(/^[5][0-9]{8}$/)]],
      depId : [null,[Validators.required]],
      employeeID : ['',[Validators.required,Validators.pattern(/^[0-9]{1,6}$/)]],
      ipPhone : ['0000']
    }
  );
  
  
  
  public user: any = {department : ''};
  selectedFile:File = null;
  validPdf = false;

  genericError= false;
  validationErrors;
  specificError;
  ous;
  deps;
  selectedOu;


    ngOnInit() {
       this._ouService.getOus().subscribe(
         data => 
            {
              console.log(data);
              this.ous = data
              this.selectedOu = data[0].key;
              this.getDepartments(this.selectedOu);         
            }
         );
       this._ouService.getDepartments('1').subscribe(data => this.deps = data);
       console.log("this ous " + this.ous);
       console.log('Document Ready OU is : ' + this.user.ou);
       this.formInitializing = false;
    }
  
    selectFile(file)
    {
      this.selectedFile = <File>file.target.files[0];
      if(this.selectedFile.name.split('.').pop().toLowerCase() == 'pdf')
			{
				this.validPdf = true;
			}else
			{
				this.validPdf =  false;
			}
    }
  
    onSubmit(userForm)
    {
      this.formInitializing = true;
      //prepare formData
      let formData = new FormData();
      formData.append('attachment',this.selectedFile,this.selectedFile.name);
      formData.append('userDetailsJson',JSON.stringify(userForm.value));
      this._userService.createUserRequest(formData).subscribe(
        data=>
        { 
          console.log(data);
          this.submitted = true;
          this.toValidate = true;
          this.returnedReqId = data.message;
          /*this._router.navigate(
            ['validate',data.message],
            {
              relativeTo: this._route
            }
          );*/
        },
      error =>
      {
          this.submitted = false;
          this.formInitializing = false;
          let httpError =  <HttpErrorResponse>error;
          if (httpError.status != 400)
          {
            this.genericError = true;
          }else
          {
            if(error.error.errorsWrapper != null)
            {
              this.validationErrors = error.error.errorsWrapper.errors
              console.log(this.validationErrors);
            }else
            {
              this.specificError = error.error.message;
            }
          }
          console.log(httpError.error);
      });
      //console.log(userForm);
    }
  
    ouChanged(ou)
    {
      console.log('after filling the OU : ' + ou);
      this.selectedOu= ou.target.value;
      this.getDepartments(this.selectedOu);
      console.log(ou.target.value);
    }
  
    getDepartments(ouId)
    {
      this._ouService.getDepartments(ouId).subscribe(data => this.deps = data);
    }

}
