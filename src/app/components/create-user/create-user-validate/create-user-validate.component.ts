import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-user-validate',
  templateUrl: './create-user-validate.component.html',
  styleUrls: ['./create-user-validate.component.css']
})
export class CreateUserValidateComponent implements OnInit {
  constructor(private _router : Router,private _route : ActivatedRoute, private fb: FormBuilder,private _userService :UserService) { }

  errorMsg;
  @Input('returnedReqId') public reqId;
  @Output('completedEvent') public completedEvent = new EventEmitter();
  @Output('validateEvent') public validateEvent = new EventEmitter();
  
  
  genericError= false;
  validateForm = this.fb.group(
    {
      vcode : ['',[Validators.required,Validators.pattern(/^[0-9]{5}$/)]]
    }
  );

  ngOnInit() {
  }

  onSubmit(event)
  {
    let formData = new FormData();
    formData.append('id',this.reqId);
    formData.append('vcode',this.validateForm.get('vcode').value);
    this._userService.validateCreateUserRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.errorMsg = null;
        this.hideValidate();
        this.setCompleted();
      },
    error =>
    {
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.genericError = true;
        }else
        {
          this.errorMsg = httpError.error.message;
        }
             
    });
    
  }

  hideValidate()
  {
    this.validateEvent.emit(false)
  }
  setCompleted()
  {
    this.completedEvent.emit(true);
  }
}
