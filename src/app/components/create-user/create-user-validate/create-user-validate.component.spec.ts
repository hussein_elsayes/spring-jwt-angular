import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUserValidateComponent } from './create-user-validate.component';

describe('CreateUserValidateComponent', () => {
  let component: CreateUserValidateComponent;
  let fixture: ComponentFixture<CreateUserValidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateUserValidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUserValidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
