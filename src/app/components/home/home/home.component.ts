import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _authenticationService: AuthenticationService) { }

  loggedIn = false;
  ngOnInit() {
    if(this._authenticationService.loggedIn())
    {
      this.loggedIn = true;
    }else
    {
      this.loggedIn = false;
    }
  }

}
