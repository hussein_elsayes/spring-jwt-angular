import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserDetails } from 'src/app/models/user-details';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-my-info',
  templateUrl: './my-info.component.html',
  styleUrls: ['./my-info.component.css']
})
export class MyInfoComponent implements OnInit {

  constructor(private _userService :UserService) { }
  formInitializing = false;
  _userDetails = new UserDetails();
  errorMsg;

  ngOnInit() {
    this.formInitializing = true;
    this._userService.getAdUserDetails().subscribe
    (data =>
      {
        this.formInitializing = false; 
        console.log(data);
        this._userDetails = data;
       
      },
      error => 
      {
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.errorMsg = "Error Executing the request !";
        }else
        {
          this.errorMsg = "Error Executing the request !";
        }
      });
  }
}
