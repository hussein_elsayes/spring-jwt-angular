import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../../models/login-model';
import {Router} from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _router: Router, private auth : AuthenticationService, private fb: FormBuilder){}
  loginForm = this.fb.group(
    {
      username : ['',[Validators.required]],
      password : ['',[Validators.required]]
    }
  );
  error = null;
  genericError= false;
  submitted:boolean = false;

  user : LoginModel = new LoginModel('','');
  ngOnInit() {}

  onSubmit()
  {
    console.log('Login Pressed !');
    this.submitted = true;
    this.auth.login(this.loginForm.value).subscribe(
      data=>
      {
        localStorage.setItem('token',data.token);
        localStorage.setItem('role',data.role);
        this._router.navigate(['/home']);
        location.reload(true);
      },
      error => {
        this.submitted = false;
        let httpError =  <HttpErrorResponse>error;
        if (httpError.status != 400)
        {
          console.log(httpError);
          this.genericError = true;
        }else
        {
          this.error = httpError.error.message;
        }
      });
  }

}
