import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private _authenticationService : AuthenticationService) { }

  loggedIn = false;

isAdmin = false;

  @Input('rtl') public rtl;
  ngOnInit() {
    if(this._authenticationService.loggedIn())
    {
      this.loggedIn = true;
    }else
    {
      this.loggedIn = false;
    }

    if(this._authenticationService.isAdmin())
    {
      this.isAdmin = true;
    }else
    {
      this.isAdmin = false;
    }
  }

}
