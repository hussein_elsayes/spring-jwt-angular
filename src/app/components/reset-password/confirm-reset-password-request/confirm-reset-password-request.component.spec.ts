import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmResetPasswordRequestComponent } from './confirm-reset-password-request.component';

describe('ConfirmResetPasswordRequestComponent', () => {
  let component: ConfirmResetPasswordRequestComponent;
  let fixture: ComponentFixture<ConfirmResetPasswordRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmResetPasswordRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmResetPasswordRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
