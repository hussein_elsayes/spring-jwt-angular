import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { PasswordValidator } from 'src/app/validators/password-validator';



@Component({
  selector: 'app-confirm-reset-password-request',
  templateUrl: './confirm-reset-password-request.component.html',
  styleUrls: ['./confirm-reset-password-request.component.css']
})
export class ConfirmResetPasswordRequestComponent implements OnInit {
  constructor(private _router : Router,private _route : ActivatedRoute, private fb: FormBuilder,private _userService :UserService) { }
  
  errorMsg;

  @Input('selectedReqId') public selectedReqId;
  @Output('hideConfirmEvent') public hideConfirmEvent = new EventEmitter();
  @Output('showSuccessEvent') public showSuccessEvent= new EventEmitter();

  validateForm = this.fb.group(
    {
      vcode : ['',[Validators.required,Validators.pattern(/^[0-9]{5}$/)]],
      password : ['',[Validators.required]],
      confirmPassword : ['',[Validators.required]]
    }
    ,{validator : PasswordValidator});
  

  ngOnInit() {
  }

  onSubmit(event)
  {
    console.log(this.selectedReqId);
    let formData = new FormData();
    formData.append('id',this.selectedReqId);
    formData.append('vcode',this.validateForm.get('vcode').value);
    formData.append('password',this.validateForm.get('password').value);
    formData.append('confirmPassword',this.validateForm.get('confirmPassword').value);
    this._userService.conrirmResetPassword(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.errorMsg = null;
        this.hideConfirmEvent.emit(false);
        this.showSuccessEvent.emit(true);
      },
    error =>
    {
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.errorMsg = "Error Executing the request !";
        }else
        {
          this.errorMsg = httpError.error.message;
        }           
    });
    
  }
}
