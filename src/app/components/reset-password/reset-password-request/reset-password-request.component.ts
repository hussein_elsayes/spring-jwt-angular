import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { OuService } from '../../../services/ou.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password-request',
  templateUrl: './reset-password-request.component.html',
  styleUrls: ['./reset-password-request.component.css']
})
export class ResetPasswordRequestComponent implements OnInit {
  constructor(private fb:FormBuilder, private _userService :UserService,private _ouService:OuService,private _router : Router, private _route :ActivatedRoute) { }
  
errorMsg;
genericError= false;

formInitializing = true;
showResetPassRequest = true;
showConfirm = false;
showSuccess = false;

selectedReqId;

//create the formGroup
userForm = this.fb.group(
  {    
    samAccount : ['',[Validators.required]],
    employeeId : ['',[Validators.required,Validators.pattern(/^[0-9]{1,6}$/)]],
    mobileNo : ['',[Validators.required,Validators.pattern(/^[5][0-9]{8}$/)]]
  }
);

  ngOnInit() {
    this.formInitializing = false;
  }

  onSubmit(userForm)
  {
    this.formInitializing = true;
    //prepare formData
    let formData = new FormData();
    formData.append('samAccount',userForm.get('samAccount').value);
    formData.append('employeeId',userForm.get('employeeId').value);
    formData.append('mobileNo',userForm.get('mobileNo').value);
    this._userService.sendResetPassRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.selectedReqId = data.message;
        this.showResetPassRequest = false;
        this.showConfirm = true;
      },
    error =>
    {
      this.formInitializing = false;
        let httpError =  <HttpErrorResponse>error;
        if (httpError.status != 400)
        {
          this.genericError =true;
        }else
        {
          this.errorMsg = httpError.error.message;
          console.log(httpError);
        }
        console.log(httpError.error);
    });
    //console.log(userForm);
  }
}
