import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserDetails } from 'src/app/models/user-details';
import { FileServiceService } from 'src/app/services/file-service.service';
import {saveAs} from 'file-saver';

@Component({
  selector: 'app-confirm-create-user',
  templateUrl: './confirm-create-user.component.html',
  styleUrls: ['./confirm-create-user.component.css']
})
export class ConfirmCreateUserComponent implements OnInit {
  constructor(private _router : Router,private _route : ActivatedRoute, private fb: FormBuilder,private _userService :UserService, private _fileService : FileServiceService) { }

  errorMsg;
  submitted = false;
  _userDetails = new UserDetails();

  @Input('selectedReq') public selectedReq;
  @Output('hideCreateUserEvent') public hideCreateUserEvent = new EventEmitter();
  @Output('confirmCreateUserEvent') public confirmCreateUserEvent = new EventEmitter(); 



  ngOnInit() {
    this._userService.getDbUserDetails(this.selectedReq).subscribe
    (data =>
      { 
        this._userDetails = data.reqDetails;
       
            },
      error => 
      {
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.errorMsg = "Error Executing the request !";
        }else
        {
          this.errorMsg = httpError.error.message;
        }
      });
  }

  createForm = this.fb.group(
    {
      samaccount : ['']
    }
  );

  onSubmit()
  {
    console.log(this.selectedReq);
    let formData = new FormData();
    formData.append('reqId',this.selectedReq);
    formData.append('samaccount',this.createForm.get('samaccount').value);
    this._userService.confirmCreateUserRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.confirmCreateUserEvent.emit(true);
        this.hideCreateUserEvent.emit(false);
      },
    error =>
    {
        let httpError =  <HttpErrorResponse>error;
        if (httpError.status == 400)
        {
          this.errorMsg = httpError.error.message;
          
        }else
        {
          this.errorMsg = error;
        }            
    });
    
  }

  denyRequest()
  {
    let formData = new FormData();
    formData.append('reqId',this.selectedReq);
    this._userService.denyRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.errorMsg = null;
        this.submitted = true;
        //show request denied successfully
      },
    error =>
    {
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.errorMsg = "Error Executing the request !";
        }else
        {
          this.errorMsg = httpError.error.message;
        }     
    });
  }

  downloadAttachment()
  {
    let formData = new FormData();
    console.log(this.selectedReq);
    formData.append('reqId',this.selectedReq);
    this._fileService.getCreateUserRequestAttachment(formData).subscribe(
      data => saveAs(data,'Attachment'),
      error => console.log(error)
    )

  }
}
