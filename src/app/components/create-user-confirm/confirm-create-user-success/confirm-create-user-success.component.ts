import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm-create-user-success',
  templateUrl: './confirm-create-user-success.component.html',
  styleUrls: ['./confirm-create-user-success.component.css']
})
export class ConfirmCreateUserSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
