import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmCreateUserSuccessComponent } from './confirm-create-user-success.component';

describe('ConfirmCreateUserSuccessComponent', () => {
  let component: ConfirmCreateUserSuccessComponent;
  let fixture: ComponentFixture<ConfirmCreateUserSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmCreateUserSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmCreateUserSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
