import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmCreateUserPasswordComponent } from './confirm-create-user-password.component';

describe('ConfirmCreateUserPasswordComponent', () => {
  let component: ConfirmCreateUserPasswordComponent;
  let fixture: ComponentFixture<ConfirmCreateUserPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmCreateUserPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmCreateUserPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
