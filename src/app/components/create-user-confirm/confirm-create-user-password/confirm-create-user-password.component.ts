import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-confirm-create-user-password',
  templateUrl: './confirm-create-user-password.component.html',
  styleUrls: ['./confirm-create-user-password.component.css']
})
export class ConfirmCreateUserPasswordComponent implements OnInit {
  constructor(private _router : Router,private _route : ActivatedRoute, private fb: FormBuilder,private _userService :UserService) { }
  errorMsg;
  
  @Input('selectedReq') public selectedReq;
  @Output('showCreateUserPassEvent') public showCreateUserPassEvent = new EventEmitter();
  @Output('showCompletedRequestEvent') public showCompletedRequestEvent = new EventEmitter(); 

  passwordForm = this.fb.group(
    {
      password : ['',[Validators.required]]
    }
  );
  ngOnInit() {
  }

  onSubmit()
  {
    console.log(this.selectedReq);
    let formData = new FormData();
    formData.append('reqId',this.selectedReq);
    formData.append('password',this.passwordForm.get('password').value);
    this._userService.confirmCreateUserRequestPassword(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.showCreateUserPassEvent.emit(false);
        this.showCompletedRequestEvent.emit(true);
        this.errorMsg = null;
      },
    error =>
    {
        let httpError =  <HttpErrorResponse>error;
        console.log(httpError);
        if (httpError.status != 400)
        {
          this.errorMsg = "Error Executing the request !";
        }else
        {
          this.errorMsg = httpError.error.message;
        }
             
    });
    
  }
}
