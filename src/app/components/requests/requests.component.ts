import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { OuService } from '../../services/ou.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  constructor(private fb:FormBuilder, private _userService :UserService,private _ouService:OuService,private _router : Router, private _route :ActivatedRoute) { }

  requests;
  errorMsg;
  selectedReqId;
  showRequests = true;
  showConfirmCreate = false;
  showConfirmCreatePass = false;
  showCompletedRequest = false;

  ngOnInit()
  {
    this._userService.getCreateUserRequests().subscribe(data => this.requests = data,
      error =>
      {
          let httpError =  <HttpErrorResponse>error;
          if (httpError.status != 400)
          {
            this.errorMsg = "Error Executing the request !";
          }
          console.log(httpError.error);
      });
  }

  setRequestId(object)
  {
    this.selectedReqId = object.srcElement.parentElement.parentElement.children[0].innerText;
    this.showRequests = false;
    this.showConfirmCreate= true;
    console.log(this.selectedReqId);
  }

  showCreateCompleted(){

  }

  setDefaults()
  {
    this.showRequests = true;
    this.showConfirmCreate = false;
    this.showConfirmCreatePass = false;
  }

}
