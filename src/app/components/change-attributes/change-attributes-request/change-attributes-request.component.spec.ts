import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeAttributesRequestComponent } from './change-attributes-request.component';

describe('ChangeAttributesRequestComponent', () => {
  let component: ChangeAttributesRequestComponent;
  let fixture: ComponentFixture<ChangeAttributesRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeAttributesRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeAttributesRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
