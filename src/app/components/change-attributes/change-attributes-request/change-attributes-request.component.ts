import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { OuService } from 'src/app/services/ou.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserDetails } from 'src/app/models/user-details';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-change-attributes-request',
  templateUrl: './change-attributes-request.component.html',
  styleUrls: ['./change-attributes-request.component.css']
})
export class ChangeAttributesRequestComponent implements OnInit {
  constructor(private fb:FormBuilder, private _userService :UserService,private _ouService:OuService,private _router : Router, private _route :ActivatedRoute) { }
  genericError = false;
  specificError;
  formInitializing = true;
  validationErrors;
  ouDepIds={};
  ous;
  deps;
  selectedOu;
  selectedDep;
  adDep;
  _userDetails = new UserDetails();


  showChangeAttrRequest = true;
  showConfirm = false;
  showSuccess=false;

  selectedReqId;
//create the formGroup
userForm = this.fb.group(
  {
    samAccount : [''],    
    firstName : ['', [Validators.required,Validators.minLength(3),Validators.maxLength(10),Validators.pattern(/^[A-z ]+$/)]],
    lastName : ['',[Validators.required,Validators.minLength(3),Validators.maxLength(10),Validators.pattern(/^[A-z ]+$/)]],
    arabicName : ['',[Validators.required,Validators.minLength(3),Validators.maxLength(10),Validators.pattern(/^((:?\s*[\u0600-\u06FF]\s*)+)$/)]],
    gender : ['',[Validators.required,Validators.pattern(/^[1-2]$/)]],
    nationalId : ['',[Validators.required,Validators.pattern(/^[1-2][0-9]{9}$/)]],
    mobile : ['',[Validators.required,Validators.pattern(/^[5][0-9]{8}$/)]],
    depId : ['',[Validators.required]],
    ouId : [''],
    employeeID : ['',[Validators.required,Validators.pattern(/^[0-9]{1,6}$/)]],
    ipPhone : ['',[Validators.required,Validators.pattern(/^[0-9]{4}$/)]]
  }
);

  ngOnInit() {
    //get AD user Details
    
    this.getAdUserDetails();
    
  }

  getAdUserDetails()
  {
    
    this._userService.getAdUserDetails().subscribe
    (data =>
      {
        
        this._userDetails = data;
        console.log(this._userDetails);
        console.log(this.userForm.value);
        this.userForm.patchValue(this._userDetails);
        this.selectedOu = this._userDetails.ouId;
        this.userForm.get('ouId').setValue(this.selectedOu);
        this.selectedDep = this._userDetails.depId;
        this.userForm.get('depId').setValue(this.selectedDep);
        this.fillOusDeps(this.selectedOu);
        this.formInitializing = false;
      },
      error => 
      { 
        let httpError =  <HttpErrorResponse>error;
        console.log(error);
        if (httpError.status != 400)
        {
        this.genericError = true;
        }else
        {
          this.validationErrors = error.error.errorsWrapper.errors
          console.log(this.validationErrors);
        }
      });
  }

  fillOusDeps(selectedOu)
  {
    this._ouService.getOus().subscribe
    (
      data => 
      {
        this.ous = data
        this.getDepartments(selectedOu);         
      }
    )
  }

  ouChanged(ou)
  {
    console.log('after filling the OU : ' + ou);
    this.selectedOu= ou.target.value;
    this.getDepartments(this.selectedOu);
    console.log(ou.target.value);
  }

  getDepartments(ouId)
  {
    this._ouService.getDepartments(ouId).subscribe(data => this.deps = data);
  }

  isSelectedOu(ou)
  {
    if(this.selectedOu === ou.key)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  isSelectedDep(dep)
  {
    if(this.selectedDep === dep.key)
    {
      return true;
    }
    else
    {
      return false;
    }
  }


  onSubmit(userForm)
  {
    this.formInitializing = true;
    this._userDetails = this.userForm.value;
    //this._userDetails.department = this.userForm.get('depId').value;
    console.log(this._userDetails);
    this._userService.ChangeUserAttributesRequest(userForm.value).subscribe(
      data=>
      {
        
        console.log(data);
        this.selectedReqId = data.message;
        this.showChangeAttrRequest = false;
        this.showConfirm = true;
      },
    error =>
    {
      this.formInitializing = false;
        let httpError =  <HttpErrorResponse>error;
        if (httpError.status != 400)
        {
          console.log(httpError);
          this.genericError = true;
        }else
        {
          if(error.error.errorsWrapper != null)
            {
              this.validationErrors = error.error.errorsWrapper.errors
              console.log(this.validationErrors);
            }else
            {
              this.specificError = error.error.message;
            }
        }
        console.log(httpError.error);
    });
    //console.log(userForm);
  }

  setDefaults()
  {
    this.showChangeAttrRequest = true;
    this.showConfirm = false;
    this.showSuccess=false;
  }
}
