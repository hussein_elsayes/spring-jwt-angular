import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeAttributesConfirmComponent } from './change-attributes-confirm.component';

describe('ChangeAttributesConfirmComponent', () => {
  let component: ChangeAttributesConfirmComponent;
  let fixture: ComponentFixture<ChangeAttributesConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeAttributesConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeAttributesConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
