import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-change-attributes-confirm',
  templateUrl: './change-attributes-confirm.component.html',
  styleUrls: ['./change-attributes-confirm.component.css']
})
export class ChangeAttributesConfirmComponent implements OnInit {
  constructor(private _router : Router,private _route : ActivatedRoute, private fb: FormBuilder,private _userService :UserService) { }

  errorMsg;

  @Input('selectedReqId') public selectedReqId;
  @Output('showConfirmEvent') public showConfirmEvent = new EventEmitter();
  @Output('showSuccessEvent') public showSuccessEvent = new EventEmitter();
  

  submitted = false;
  formInitializing = false;

  validateForm = this.fb.group(
    {
      vcode : ['']
    }
  );

  ngOnInit() {
  }

  onSubmit(event)
  {
    this.formInitializing = true;
    console.log(this.selectedReqId);
    let formData = new FormData();
    formData.append('id',this.selectedReqId);
    formData.append('vcode',this.validateForm.get('vcode').value);
    this._userService.confirmChangeUserAttributesRequest(formData).subscribe(
      data=>
      { 
        console.log(data);
        this.errorMsg = null;
        this.showConfirmEvent.emit(false);
        this.showSuccessEvent.emit(true);  
      },
    error =>
    {
        //let httpError =  <HttpErrorResponse>error;
        console.log(error);
        //if (httpError.status != 400)
        //{
          this.errorMsg = error;
        //}else
        //{
          //this.errorMsg = httpError.error.message;
        //}
             
    });
    
  }
}
