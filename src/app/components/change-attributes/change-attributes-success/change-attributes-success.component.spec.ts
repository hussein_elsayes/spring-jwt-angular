import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeAttributesSuccessComponent } from './change-attributes-success.component';

describe('ChangeAttributesSuccessComponent', () => {
  let component: ChangeAttributesSuccessComponent;
  let fixture: ComponentFixture<ChangeAttributesSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeAttributesSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeAttributesSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
