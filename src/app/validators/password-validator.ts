import { AbstractControl } from "@angular/forms";

export function PasswordValidator(control : AbstractControl) 
{
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');
    if(password.value != confirmPassword.value && password.dirty && confirmPassword.dirty)
    {
        console.log('mismatch');
        return {'mismatch':true};
    }else
    {
        return null;
    }
}