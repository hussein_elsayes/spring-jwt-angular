import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RequestsComponent } from './components/requests/requests.component';
import { CreateUserComponent } from './components/create-user/create-user/create-user.component';
import { ChangeAttributesRequestComponent } from './components/change-attributes/change-attributes-request/change-attributes-request.component';
import { ResetPasswordRequestComponent } from './components/reset-password/reset-password-request/reset-password-request.component';
import { ChangePasswordRequestComponent } from './components/change-password/change-password-request/change-password-request.component';
import { AuthGuard } from './guards/auth.guard';
import { TrackPendingRequestComponent } from './components/track-pending/track-pending-request/track-pending-request.component';
import { HomeComponent } from './components/home/home/home.component';
import { LoadingComponent } from './components/loading/loading.component';


const routes: Routes = [
  {path:'', redirectTo : '/home', pathMatch:'full'},
  {path:'resetPass', component : ResetPasswordRequestComponent},
  {path:'loading', component : LoadingComponent},
  {path:'login', component : LoginComponent},
  {path:'createUser', component: CreateUserComponent},
  //{path:'trackPending', component: TrackPendingRequestComponent},
  {
    path:'home',
    component : HomeComponent,
    canActivate : [AuthGuard]
  },      
  {
    path:'attributes',
    component : ChangeAttributesRequestComponent,
    canActivate : [AuthGuard]
  },
  {
    path:'admin',
    component : RequestsComponent,
    canActivate : [AuthGuard]
  },
  {
    path:'changePass',
    component : ChangePasswordRequestComponent,
    canActivate : [AuthGuard]
  },
  
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
