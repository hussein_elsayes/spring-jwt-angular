import { TestBed } from '@angular/core/testing';

import { UnauthorizedErrorInterceptorService } from './unauthorized-error-interceptor.service';

describe('UnauthorizedErrorInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnauthorizedErrorInterceptorService = TestBed.get(UnauthorizedErrorInterceptorService);
    expect(service).toBeTruthy();
  });
});
