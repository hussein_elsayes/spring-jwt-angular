import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { LocaleService } from '../services/locale.service';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

  constructor(private _authService : AuthenticationService, private _localeService: LocaleService) { }

  intercept( request :HttpRequest<any>, handler:HttpHandler) : Observable<HttpEvent<any>>
  {
    console.log('calling JWT Interceptor ');
    request = request.clone(
      {
        setHeaders: {
          'Authorization': 'Bearer ' + this._authService.getToken(),
          'Accept-Language' : this._localeService.getLocale()}
      }
    );
    console.log('current locale : ' + this._localeService.getLocale());
    return handler.handle(request);
  }
}
