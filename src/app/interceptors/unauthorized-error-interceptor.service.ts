import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UnauthorizedErrorInterceptorService implements HttpInterceptor {

  constructor(private _authService: AuthenticationService, private _router:Router) { }

  intercept(request :HttpRequest<any>, handler : HttpHandler) : Observable<HttpEvent<any>>
  {
    console.log('Calling UnAuthorized Interceptor !')
    return handler.handle(request).pipe(catchError(
      error => 
      {
        console.log('Inside Error Piping --> UnAuthorized Interceptor !')
        let httpError:HttpErrorResponse = <HttpErrorResponse>error;
        if(httpError.status == 401)
        {
          console.error(httpError);
          this._authService.logOut();
          this._router.navigate(['/login']);
          location.reload(true);
        }
        return throwError(httpError);
      }
    ));
  }
}
