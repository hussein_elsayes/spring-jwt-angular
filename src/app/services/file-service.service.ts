import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';
import { TagContentType } from '@angular/compiler';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileServiceService {

  private _url = environment.backendURL + "/api";
  constructor(private _http : HttpClient) { }

  getCreateUserRequestAttachment(formData)
  {
    return this._http.post<any>(this._url+'/requests/auth/downloadAttachment',formData
    ,{
      responseType : 'blob' as 'json'
    });
  }
}
