import { TestBed } from '@angular/core/testing';

import { OuService } from './ou.service';

describe('OuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OuService = TestBed.get(OuService);
    expect(service).toBeTruthy();
  });
});
