import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OuService {

  constructor(private _http : HttpClient) { }

  baseUrl= environment.backendURL +'/api/ous';
  getOus()
  {
    return this._http.get<any>(this.baseUrl);
  }
  getDepartments(ouId)
  {
    return this._http.get<any>(this.baseUrl+'/'+ouId);
  }
}
