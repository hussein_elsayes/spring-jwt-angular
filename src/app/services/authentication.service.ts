import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LoginModel } from '../models/login-model';
import {catchError} from 'rxjs/operators/';
import {throwError} from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private _http: HttpClient) { }

  private _url = environment.backendURL + "/api";

  login(formData)
  {
    return this._http.post<any>(this._url+'/signin',formData).pipe(catchError(this.handleError));
  }

  loggedIn()
  {
    return !!localStorage.getItem('token');
  }

  isAdmin()
  {
    var role = localStorage.getItem('role');
    if ( role == "ROLE_EMPADM")
    {
      return true; 
    }
    return false;
  }

  getToken()
  {
    return localStorage.getItem('token');
  }

  logOut()
  {
    localStorage.removeItem('token');
  }
  handleError(error: HttpErrorResponse)
  {
    return throwError(error);
  }
}
