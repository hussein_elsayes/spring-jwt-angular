import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocaleService {

  constructor() { }

  getLocale()
  {
    let rtl = localStorage.getItem('rtl');
    if(rtl == 'true')
    {
      return 'ar-SA';
    }
    else
    {
      return 'en-US';
    }
  }

  isRtl()
  {
    return localStorage.getItem('rtl');
  }
}
