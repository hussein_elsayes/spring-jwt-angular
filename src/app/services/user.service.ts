import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { UserDetails } from '../models/user-details';
import { environment } from 'src/environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _url = environment.backendURL +"/api";
  constructor(private _http : HttpClient) { }

  getCreateUserRequests()
  {
    return this._http.get<any>(this._url+'/requests/auth').pipe(catchError(this.handleError));
  }
  
  getDbUserDetails(reqId)
  {
    return this._http.get<any>(this._url+'/requests/auth/'+reqId).pipe(catchError(this.handleError));
  }

  getAdUserDetails()
  {
    return this._http.get<any>(this._url+'/users/auth/').pipe(catchError(this.handleError));
  }


  //get pending request
  getPendingRequest(formData)
  {
    return this._http.post<any>(this._url+'/requests/pending',formData).pipe(catchError(this.handleError));
  }

  //create user request
  createUserRequest(formData)
  {
    return this._http.post<any>(this._url+'/requests/createUser',formData).pipe(catchError(this.handleError));
  }

  //validate create user request
  validateCreateUserRequest(formData)
  {
    return this._http.post<any>(this._url+'/requests/validateCreateUser',formData).pipe(catchError(this.handleError));
  }

  //confirm create user request
  confirmCreateUserRequest(formData)
  {
    return this._http.post<any>(this._url+'/users/auth/create',formData).pipe(catchError(this.handleError));
  }

  //confirm create user request password
  confirmCreateUserRequestPassword(formData)
  {
    return this._http.post<any>(this._url+'/users/auth/createPass',formData).pipe(catchError(this.handleError));
  }

  //deny create user request
  denyRequest(formData)
  {
    return this._http.post<any>(this._url+'/requests/auth/deny',formData).pipe(catchError(this.handleError));
  }

  //change user attributes request
  ChangeUserAttributesRequest(userDetails)
  {
    return this._http.post<any>(this._url+'/requests/auth/changeAttr',userDetails).pipe(catchError(this.handleError));
  }

  //confirm change user attributes request
  confirmChangeUserAttributesRequest(formData)
  {
    return this._http.post<any>(this._url+'/users/auth/changeAttr',formData).pipe(catchError(this.handleError));
  }

  //change pass request
  sendChangePassRequest()
  {
  return this._http.post<any>(this._url+'/requests/auth/changePass/',null).pipe(catchError(this.handleError));
  }

  //confirm change pass request
  confirmChangePassRequest(formData)
  {
  return this._http.post<any>(this._url+'/users/auth/changePass/',formData).pipe(catchError(this.handleError));
  }

  //reset pass request
  sendResetPassRequest(formData)
  {
    return this._http.post<any>(this._url+'/requests/resetPass/',formData).pipe(catchError(this.handleError));
  }

  //reset pass confirm
  conrirmResetPassword(formData)
  {
    return this._http.post<any>(this._url+'/users/resetPass',formData).pipe(catchError(this.handleError));
  }



  handleError(error : HttpErrorResponse)
  {
    return throwError(error);
  }
}
